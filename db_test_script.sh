#!/bin/bash

while
    sleep 5
    docker exec db bash -c 'mysql -u "$MYSQL_USER" --password="$MYSQL_PASSWORD" -e "SHOW DATABASES LIKE '"'"'hobbie_db'"'"';"'
    [ $? -ne 0 ]
do true; done
